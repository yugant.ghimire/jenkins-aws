import groovy.transform.Field

/* ***

固定の値
***/
@Field public Map gitlab_info = [
    host: 'gitlab.aslead.cloud',
    branch: 'NPL-605'    // STG用
//    branch: 'main'    // 本番用
]

@Field public Map s3_info = [
    url: 's3://aslead-search-staging'    // STG用
//    url: 's3://aslead-search-production'    // 本番用
]

return this