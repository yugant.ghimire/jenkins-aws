pipeline {
    agent none
    stages {
        stage('Build') {
            agent {
                docker {
                    image 'python:3'
                }
            }
            steps {
                sh 'python sources/add2vals.py sources/calc.py'
            }
        }
    }
    stages {
        stage('Copy to S3') {
            steps {
                script {
                    def awsCreds = Jenkins.instance.credentials.find {
                        it instanceof AmazonWebServicesCredentials && it.getDisplayName() == 'local-aws-credentials'
                    }
                    
                    def accessKeyId = awsCreds.getAWSAccessKeyId()
                    def secretAccessKey = awsCreds.getAWSSecretKey()
                    
                    withAWS(region: 'eu-north-1', credentials: awsCreds) {
                        sh 'touch myFile.txt'
                        sh 'aws s3 cp myFile.txt s3://my-bucket'
                    }
                }
            }
        }
    }
    post { 
        cleanup { 
            deleteDir()
        }
/
        failure {
            mattermostSend (
                color: "danger", 
                message: "Build FAILED: ${env.JOB_NAME} #${env.BUILD_NUMBER} (<${env.BUILD_URL}|Link to build>)"
            )
        }
*/
    }
}

